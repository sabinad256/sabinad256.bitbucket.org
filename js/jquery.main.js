var $ = jQuery;
jQuery(document).ready(function(){
	$('html').removeClass('no-js');

	//Burger menu
	$('#nav .opener').click(function(e){
		$(this).closest('#nav').toggleClass('active');
		$('body').toggleClass('nav-active');
		e.preventDefault();
	});

	$('.overlay').click(function(){
		$('body').removeClass('nav-active');
		$('#nav').removeClass('active');
	});

	//Smooth scroll from bottom to top
	$('.back-to-top').click(function(){
		$('html,body').animate({
			scrollTop: 0}, 600);
		return false;
	});

	//Side block slide
	$('.list-block .title').click(function(){
		$(this).siblings('.list-wrap').slideToggle();
		jcf.replaceAll();
	});

	//List-grid
	$('.grid-list a').click(function(e){
		gridText = $(this).text();
		$('.grid-list li').removeClass('active');
		$(this).closest('li').addClass('active');

		if(gridText == "grid") {
			$(this).closest('.flight-block').find('.list-grid').removeClass('list');
			$(this).closest('.flight-block').find('.list-grid').addClass(gridText);
		} else if(gridText == "list") {
			$(this).closest('.flight-block').find('.list-grid').removeClass('grid');
			$(this).closest('.flight-block').find('.list-grid').addClass(gridText);
		}
		e.preventDefault();
	});

	if($(window).width() < 768){
		if($('.list-grid').hasClass('list')){
			$('.list-grid').removeClass('list');
		}
	}

	//select for filter
	$('.filter-block ul a').click(function(e){
		if($(window).width() < 768){
			var fil_select = $(this).text();
			$('.filter-block .selected').text(fil_select);
			$('.filter-block ul').slideUp();
			e.preventDefault();
		}
	});

	$('.selected').click(function(e){
		if($(window).width() < 768){
			$(this).siblings('ul').slideToggle();
			e.preventDefault();
		}
	})

	if($(window).width()>1024){
		$('#nav').removeClass('active');
	}

	//Star rating
	$('.star-rate a').click(function(e){
		$(this).parent('li').toggleClass('rated');
		// if($('.star-rate li').hasClass('rated')){

		// }else{

		// }
		e.preventDefault();
	});
});

$(window).on('load', function() { 
	//Remove Loader
	// $('body').toggleClass('loaded');

	// $('#loader').fadeOut();
	// $('#loader-wrapper').delay(350).fadeOut('slow'); 
	$('#loader-wrapper').remove(); 
});


$(window).resize(function(){
	if($(window).width()<768){
		if($('.list-grid').hasClass('list')){
			$('.list-grid').removeClass('list');
		}
	}

	if($(window).width() > 768){
		$('.filter-heading .filter-block ul').removeAttr('style');
	}

	if($(window).width()>1024){
		$('#nav').removeClass('active');
	}
});

$(window).scroll(function(){
	if($(window).scrollTop() > 50){
		$('.back-to-top').fadeIn();
	}else{
		$('.back-to-top').fadeOut();
	}
});

